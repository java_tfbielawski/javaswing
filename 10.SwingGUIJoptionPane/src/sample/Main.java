/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 10: Dialog Boxes
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo dialog boxes
 * Main.java
 * */

package sample;
import javax.swing.*;
import javax.swing.ImageIcon;


public class Main
{
    public static void main(String[] args)
    {
        //Create the dialog
        //Pass in component/message/title/message type
        JOptionPane.showMessageDialog(null, "This is a Plain Message",
                "Dialog Title Here",JOptionPane.PLAIN_MESSAGE);
        JOptionPane.showMessageDialog(null, "This is an Information Message!",
                "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, "This is a Question Message?",
                "Dialog Title Here",JOptionPane.QUESTION_MESSAGE);
        //Contain any dialog in a while loop to prevent the box from ever disappearing
        // (but really don't do that because its not very nice.)
        JOptionPane.showMessageDialog(null, "Your computer has been infected by 63 viruses!!!!",
                "Dialog Title Here",JOptionPane.WARNING_MESSAGE);
        JOptionPane.showMessageDialog(null, "This is an Error Message!",
                "Dialog Title Here",JOptionPane.ERROR_MESSAGE);

        //Show confirm dialog
        //It does nothing like this
//        JOptionPane.showConfirmDialog(null, "This is a YES/NO/Cancel box.",
//                "Title Goes Here", JOptionPane.YES_NO_CANCEL_OPTION);

        //Launch the dialog from println(). Yes returns 0, No returns 1, Cancel returns 2, X returns -1
        System.out.println(JOptionPane.showConfirmDialog(null, "This is a YES/NO/Cancel box.",
                "Title Goes Here", JOptionPane.YES_NO_CANCEL_OPTION));

        //Declare string variable, it shall hold the input from this dialog
        String name = JOptionPane.showInputDialog("Please enter your name: ");
        JOptionPane.showMessageDialog(null, "Hello " + name + "!",
                "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);

        //Add options for the buttons using an array of strings
        String[] responses = {"Option 1", "Option 2", "Option 3"};

        //Set icon for dialog box
        ImageIcon icon = new ImageIcon("cpl.png");

        //Combination of above dialogs
        JOptionPane.showOptionDialog(null,"Awesome Combo Dialog",
                "Title goes here", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE,
                icon,responses,0);



    }
}
