/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 9: New Window
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo creating a new window
 * NewWindow.java (NewWindow class defining the new window)
 * */

package sample;

import javax.swing.*;
import java.awt.*;

public class NewWindow
{
    //Create Frame
    JFrame frame = new JFrame("Hello");

    //Label
    JLabel label = new JLabel("There");


    //Constructor
    NewWindow()
    {
        /* Label Details */
        label.setBounds(0,0,100,50);
        //Set the font, pass in font name/style/size
        label.setFont(new Font(null, Font.PLAIN,25));
        //add to the frame
        frame.add(label);
        /* Frame details */
        //Set close ops
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set frame size
        frame.setSize(500,500);
        //Set layout
        frame.setLayout(null);
        //Set visibility
        frame.setVisible(true);

    }
}
