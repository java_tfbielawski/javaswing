/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 9: New Window
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo creating a new window
 * Main.java (Main class with main method)
 * */

package sample;

public class Main
{
    public static void main(String[] args)
    {
        //Instantiate a LaunchPage class object
        LaunchPage launchPage = new LaunchPage();


    }

}
