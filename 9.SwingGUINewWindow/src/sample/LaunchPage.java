/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 9: New Window
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo creating a new window
 * LaunchPage.java (LaunchPage class to open new window)
 * */
package sample;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LaunchPage implements ActionListener
{
    //Create a frame
    JFrame frame = new JFrame();
    //Create a button for frame
    JButton myButton = new JButton("New Window");

    //Constructor
    LaunchPage()
    {
        /* Button Details */
        //Set the button boundaries
        myButton.setBounds(100,160,200,40);
        //Set focus: true highlights the button
        myButton.setFocusable(true);
        //Add an action listener for the button
        myButton.addActionListener(this);

        /* Frame details */
        //Set close ops
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set frame size
        frame.setSize(500,500);
        //Set layout
        frame.setLayout(null);
        //Set visibility
        frame.setVisible(true);
        //Add the button to the frame
        frame.add(myButton);

    }

    //Method to perform an action upon button click
    @Override
    public void actionPerformed(ActionEvent e)
    {
        //If button is clicked...
        if(e.getSource() == myButton)
        {
            //Closes the frame when button is clicked and new window launched
            frame.dispose();
            //Create an object of NewWindow class
            NewWindow myWindow = new NewWindow();
        }
    }
}
