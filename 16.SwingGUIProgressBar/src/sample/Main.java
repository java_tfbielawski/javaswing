/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 16: Progress Bar
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo a Progress Bar
 * Main.java
 * */

package sample;


public class Main
{
    //Main Method
    public static void main(String[] args)
    {
        //Instantiate ProgressBar class object
        ProgressBar barDemo = new ProgressBar();
    }
}
