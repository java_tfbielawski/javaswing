/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 16: Progress Bar
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo a Progress Bar
 * PrgressBar.java
 * */

package sample;

import java.awt.*;
import javax.swing.*;

public class ProgressBar
{
    //Declare frame and progress bar
    JFrame frame = new JFrame();
    JProgressBar pBar = new JProgressBar(0,500);

    //Class constructor
    ProgressBar()
    {
        //Set the progress bar starting value
        pBar.setValue(0);
        //Set the progress bar bounds
        pBar.setBounds(0,0,500,50);
        //% status for progress bar
        pBar.setStringPainted(true);
        //Set the progress bar font
        pBar.setFont(new Font("DejaVu serif",Font.BOLD,35));
        //Set the foreground (bar) color
        pBar.setForeground(Color.GREEN);
        //Set the bar background color
        pBar.setBackground(Color.BLACK);

        //Add the progress bar to the frame
        frame.add(pBar);
        //Set close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the size
        frame.setSize(500,500);
        //Set the layout manager
        frame.setLayout(null);
        //Set the frame visibility
        frame.setVisible(true);

        //Call the fill method for the bar
        fill();

    }

    //Method to fill the progress bar
    public void fill()
    {
        //While loop to count down from 500
        int counter = 500;
        while(counter > 0)
        {
            pBar.setValue(counter);
            try
            {
                //Pause program for 1 second after each loop iteration
                Thread.sleep(50);
            }

            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //Counter decrements by 1 during each pause
            counter -=1;
        }
        //Text for the bar
        pBar.setString("Done.");

    }
}
