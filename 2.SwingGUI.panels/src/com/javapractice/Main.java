/* *
* Tom Bielawski
* 03/11/2021
* Bro Code Java GUI Tutorial: Panels
* https://www.youtube.com/watch?v=Kmgo00avvEw
* Program to demonstrate adding JPanel to frame
* */

package com.javapractice;
import javax.swing.*;
import java.awt.*;

public class Main
{

    public static void main(String[] args)
    {
        /* Images */
        //Instantiate image
        ImageIcon image1 = new ImageIcon("sgt2.png");

        /* Label */
        //Instantiate a label
        JLabel label = new JLabel();
        //Set the label text or pass into constructor
        label.setText("SGT");
        //Set label icon
        label.setIcon(image1);
        //Set vertical
        //label.setVerticalAlignment(JLabel.BOTTOM);
        //Set horizontal
        //label.setHorizontalAlignment(JLabel.RIGHT);
        //Set boundary of label
        label.setBounds(0,0,200,200);


        /* Red Panel */
        //Instantiate a panel
        JPanel redPanel = new JPanel();
        //Set the color to red
        redPanel.setBackground(Color.RED);
        //Place panel top left
        redPanel.setBounds(0,0,250,250);
        redPanel.setLayout(null);


        /* Blue Panel */
        //Instantiate a panel
        JPanel bluePanel = new JPanel();
        //Set color to blue
        bluePanel.setBackground(Color.BLUE);
        //Place panel next to red
        bluePanel.setBounds(250,0,250,250);
        bluePanel.setLayout(null);


        /* Green Panel */
        //Instantiate a panel
        JPanel greenPanel = new JPanel();
        //Set color to green
        greenPanel.setBackground(Color.GREEN);
        //Place green panel below blue and red, make it twice as wide
        greenPanel.setBounds(0,250,500,250);
        //Border
        greenPanel.setLayout(null);



        /* Frame */
        //Instantiate frame
        JFrame frame = new JFrame("Panel Practice");
        //Set frame close operation to exit
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set layout to null
        frame.setLayout(null);
        //Set frame size
        frame.setSize(850,750);
        //Make frame visible
        frame.setVisible(true);

        /* Components */
        //Add redpanel to frame
        frame.add(redPanel);
        //Add bluepanel to frame
        frame.add(bluePanel);
        //Add greenpanel to frame
        frame.add(greenPanel);
        //Add label to the red panel
        greenPanel.add(label);


    }
}
