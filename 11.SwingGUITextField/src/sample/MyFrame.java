/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 11
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo text fields
 * MyFrame.java
 * */

package sample;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener
{
    //Fields
    JButton button;
    JTextField textField;
    //Constructor
    MyFrame()
    {
        //Set the exit ops
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the layout
        this.setLayout(new FlowLayout());

        //Create a button
        button = new JButton("SUBMIT");
        //Action listener for button trigger
        button.addActionListener(this);

        //Create the text field
        textField =  new JTextField();
        //Set the field size
        textField.setPreferredSize(new Dimension(250,40));
        //Set the text font
        textField.setFont(new Font("Deja Vu Serif", Font.BOLD,25));
        //Set font color
        textField.setForeground(Color.BLUE);
        //Set the text field background
        textField.setBackground(Color.BLACK);
        //Set the caret (cursor) color
        textField.setCaretColor(Color.WHITE);
        //Default text within the field, erase to enter text
        textField.setText("USERNAME");

        //


        //Add the button
        this.add(button);
        //Add the field
        this.add(textField);

        //Adjust frame to fit components added
        this.pack();
        //Set visibility
        this.setVisible(true);
    }

    //This method is activated when button clicked
    @Override
    public void actionPerformed(ActionEvent e)
    {
        //If button is clicked...
        if (e.getSource() == button)
        {
            //Display the text field contents in a new dialog
            JOptionPane.showMessageDialog(null, textField.getText(),
                    "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);

            //This option renders the button unclickable if false
            button.setEnabled(false);
            //Set the editable method can prevent the default text from being edited
            textField.setEditable(false);

        }
    }
}
