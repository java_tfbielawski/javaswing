/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 11: Text Fields
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo text fields
 * Main.java
 * */

package sample;
import javax.swing.JFrame;

public class Main
{
    public static void main(String[] args)
    {
	//Instantiate a MyFrame class object
        MyFrame frame = new MyFrame();

    }
}
