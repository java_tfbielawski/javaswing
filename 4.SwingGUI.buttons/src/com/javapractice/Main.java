/* *
 * JavaGUI4 Main.java
 * Tom Bielawski
 * 03/11/2021
 * Bro Code Java GUI 4 Tutorial: Buttons
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demonstrate adding JButton to frame
 * */


package com.javapractice;
import javax.swing.*;

public class Main
{

    public static void main(String[] args)
    {
        MyFrame frame = new MyFrame();

    }
}
