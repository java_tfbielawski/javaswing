/* *
 * JavaGUI4 Main.java
 * Tom Bielawski
 * 03/11/2021
 * Bro Code Java GUI 4 Tutorial: Buttons
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demonstrate adding JButton to frame
 * */
package com.javapractice;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame implements ActionListener
{
    //Declare fields
    JButton button;
    JLabel label;

    //Constructor
    MyFrame()
    {
        /* Image */
        //Instantiate the icon and pass in the name
        ImageIcon icon = new ImageIcon("sgt2.png");
        ImageIcon icon2 = new ImageIcon("usmc.jpeg");

        /* JLabel */
        label = new JLabel();
        //Set the label icon
        label.setIcon(icon2);
        //Set label boundaries
        label.setBounds(150, 250, 150, 150);

        label.setVisible(false);

        /* JButton */
        //Instantiate the button
        button = new JButton();
        //Set the boundary
        button.setBounds(200,100,250,200);
        //Button Text
        button.setText("PUSH!");
        //Set the icon
        button.setIcon(icon);
        //Button alignment
        button.setHorizontalTextPosition(JButton.CENTER);
        button.setVerticalTextPosition(JButton.BOTTOM);
        //Set new button font
        button.setFont(new Font("Comic Sans", Font.BOLD, 25));
        //Button color
        button.setForeground(Color.RED);
        //Button background
        button.setBackground(Color.LIGHT_GRAY);
        //Button border
        button.setBorder(BorderFactory.createEtchedBorder());
        //Set text gap
        button.setIconTextGap(5);
        //To disable button
        //button.setEnabled(false);
        //Disable button
        button.setFocusable(false);
        //Listen for the the button
        button.addActionListener(this);
        //Lambda expression instead of Action Listener Interface
        //button.addActionListener(e -> System.out.println("SEMPER FI!"));



        /* JFrame */
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setSize(500,500);
        this.setVisible(true);


        /* Components */
        this.add(button);
        this.add(label);

    }

    //Define the action for the button-use with action listener
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == button)
        {
            label.setVisible(true);
            //System.out.println("SEMPER FIDELIS!");
        }
    }
}
