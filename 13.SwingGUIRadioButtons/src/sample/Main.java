/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 13: Radio Buttons
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo radio buttons
 * Main.java
 * */

package sample;


public class Main
{
    //Main method
    public static void main(String[] args)
    {
        //Instantiate object of MyFrame class
        MyFrame frame = new MyFrame();
    }
}
