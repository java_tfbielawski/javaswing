/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 13: Radio Buttons 
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo radio buttons with a dialog box
 * MyFrame.java
 * */

package sample;

import java.awt.*;
import java.awt.event.*;
import javax.swing.ImageIcon;
import javax.swing.*;

//MyFrame class extends JFrame class and implements ActionListener
public class MyFrame extends JFrame implements ActionListener
{
    //Declare the buttons
    JRadioButton brookTroutButton;
    JRadioButton brownTroutButton;
    JRadioButton cutthroatTroutButton;

    //Declare Image Icons
    ImageIcon brookTrout;
    ImageIcon brownTrout;
    ImageIcon cutthroatTrout;

    //Constructor
    MyFrame()
    {
        //Set the close operation
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the layout manager
        this.setLayout(new FlowLayout());

        //Instantiate icons
        brookTrout = new ImageIcon("brook.jpeg");
        brownTrout = new ImageIcon("brown.jpeg");
        cutthroatTrout = new ImageIcon("cutty.jpeg");

        //Instantiate buttons
        brookTroutButton = new JRadioButton("Brook Trout");
        brownTroutButton = new JRadioButton("Brown Trout");
        cutthroatTroutButton = new JRadioButton("Cutthroat Trout");

        //Create a button group and add the buttons
        ButtonGroup group = new ButtonGroup();
        group.add(brookTroutButton);
        group.add(brownTroutButton);
        group.add(cutthroatTroutButton);

        //Action Listeners "listen" for event triggers
        brookTroutButton.addActionListener(this);
        brownTroutButton.addActionListener(this);
        cutthroatTroutButton.addActionListener(this);

        //Add the icons
        brookTroutButton.setIcon(brookTrout);
        brownTroutButton.setIcon(brownTrout);
        cutthroatTroutButton.setIcon(cutthroatTrout);

        //Add the buttons
        this.add(brookTroutButton);
        this.add(brownTroutButton);
        this.add(cutthroatTroutButton);


        //
        this.pack();
        //Set visibility
        this.setVisible(true);
    }


    //This method listens for the trigger event
    @Override
    public void actionPerformed(ActionEvent e)
    {
        //If Brook trout is selected
        if (e.getSource() == brookTroutButton)
        {
            //Dialog box confirms selection
            JOptionPane.showMessageDialog(null, "You chose Brook Trout!",
                    "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);
        }

        //If Brown trout is selected
        else if (e.getSource() == brownTroutButton)
        {
            //Dialog box confirms selection
            JOptionPane.showMessageDialog(null, "You chose Brown Trout!",
                    "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);
        }

        //If Cutthroat trout is selected
        else if (e.getSource() == cutthroatTroutButton)
        {
            //Dialog box confirms selection
            JOptionPane.showMessageDialog(null, "You chose Cutthroat Trout!",
                    "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);
        }

    }
}
