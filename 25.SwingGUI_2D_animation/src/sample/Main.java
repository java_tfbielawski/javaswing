/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 25: 2D animation
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo simple 2D animations
 * Main.java
 * */


package sample;


public class Main
{
    //Main Method
    public static void main(String[] args)
    {
        //Instantiate a MyFrame class object
        MyFrame frame = new MyFrame("ALIENS!");

    }
}
