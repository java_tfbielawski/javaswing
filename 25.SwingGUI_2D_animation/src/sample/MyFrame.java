/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 25: 2D animation
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo simple 2D animations
 * MyFrame.java
 * */
package sample;


import java.awt.*;
import javax.swing.*;

//MyFrame class extends the JFrame Class
public class MyFrame extends JFrame
{
    //Declare a panel
    MyPanel panel;

    //Class constructor
    MyFrame(String s)
    {
        //Instantiate a Panel class object
        panel = new MyPanel("title");

        //Set the frame close operation
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Add the panel
        this.add(panel);
        //
        this.pack();
        //Set the relative location puts frame in middle of screen
        this.setLocationRelativeTo(null);
        //Set visibility
        this.setVisible(true);
    }

}
