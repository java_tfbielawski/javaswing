/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 25: 2D animation
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo simple 2D animations
 * MyPanel.java
 * */

package sample;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.xml.crypto.dsig.XMLValidateContext;

public class MyPanel extends JPanel implements ActionListener
{
    //Declare panel dimension variables
    final int PANEL_WIDTH = 762;
    final int PANEL_HEIGHT = 762;

    //Declare image variables
    Image enemy;
    Image backgroundImage;

    //Declare the timer variable
    Timer timer;

    /*
    X and Y velocity establish how many pixels the image
    will move along each respective axis
    If both velocities are equal, icon moves only diagonally
    If one is faster than the other, icon zig-zags
    */
    int xVelocity = 5;
    int yVelocity = 1;

    //Declare x and y axis variables to hold starting positions
    int x = 0;
    int y = 0;

    //Class constructor
    MyPanel(String title)
    {
        //Set the panel size, pass in final ints
        this.setPreferredSize(new Dimension(PANEL_WIDTH,PANEL_HEIGHT));
        //Set the background color
        this.setBackground(Color.BLACK);

        //Instantiate the image icons
        enemy = new ImageIcon("invader.png").getImage();
        backgroundImage = new ImageIcon("planet.jpeg").getImage();

        //Instantiate the time, pass in int and action listener
        timer = new Timer(10,this);
        //Start the timer
        timer.start();

    }

    //Paint method
    public void paint(Graphics g) 
    {
        //Call the Super Class paint method to paint the background
        super.paint(g);
        //Cast to g as 2D graphic
        Graphics2D g2d = (Graphics2D) g;

        //Draw the enemy and background images on the panel
        g2d.drawImage(backgroundImage,0,0,null);
        g2d.drawImage(enemy,x,y,null);

    }

    //Method to force icon movement horizontally or vertically
    @Override
    public void actionPerformed(ActionEvent e)
    {
        //If the icon hits the panel boundary on the right ||
        //X < 0 (less than the icon's starting position)
        if (x >= PANEL_WIDTH - enemy.getWidth(null) || x < 0)
        {
            //This forces icon to reverse direction
            xVelocity = xVelocity * -1;
        }
        //Start the image moving along the x axis
        x =  x + xVelocity;

        //Horizontal axis movement
        if (y >= PANEL_HEIGHT - enemy.getHeight(null) || y < 0)
        {
            //This forces icon to reverse direction
            yVelocity = yVelocity * -1;
        }
        //Start the image moving along the x axis
        y =  y + yVelocity;
        //Call the repaint method after each movement
        repaint();


    }
}
