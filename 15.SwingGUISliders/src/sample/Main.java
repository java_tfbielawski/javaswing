/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 15: Slider
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo a slider
 * Main.java
 * */

package sample;


public class Main
{
    public static void main(String[] args)
    {
	//Instantiate a SliderDemo class object
        SliderDemo sliderDemo = new SliderDemo();
    }
}
