/* *
 * Tom Bielawski
 * 03/18/2021
 * Bro Code Java GUI Swing Tutorial 15: Slider
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo a slider
 * SliderDemo.java
 * */


package sample;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class SliderDemo implements ChangeListener
{
    //Declare frame, panel, label, slider
    JFrame frame;
    JPanel panel;
    JLabel label;
    JSlider slider;

    //Constructor
    SliderDemo()
    {
        //Instantiate "J" objects
        frame = new JFrame("Slider Demo");
        panel = new JPanel();
        label = new JLabel();
        //Instantiate slider and pass in range and starting point
        slider = new JSlider(0,100,50);

        //Set the close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set dimensions of the slider
        slider.setPreferredSize(new Dimension(400,200));

        //Set the slider tick marks
        slider.setPaintTicks(true);
        //Set minor tick spacing values
        slider.setMinorTickSpacing(10);
        //Set paint track
        slider.setPaintTrack(true);
        //Set major tick spacing values
        slider.setMajorTickSpacing(25);
        //Add the Major Tick Values to the slider
        slider.setPaintLabels(true);
        //Set font
        slider.setFont(new Font("Droid Serif",Font.BOLD,25));
        //Set the orientation to vertical
        slider.setOrientation(SwingConstants.VERTICAL);

        //Add label to the slider for degrees C
        label.setText("\u000B°C = "+ slider.getValue());
        //Set label font
        label.setFont(new Font("Droid Serif",Font.BOLD,35));

        //Listener to trigger the state change event
        slider.addChangeListener(this);

        //Add slider to panel
        panel.add(slider);
        panel.add(label);
        frame.add(panel);
        //Set the frame size
        frame.setSize(500,500);
        //Set visibility
        frame.setVisible(true);


    }

    //Method to execute when state changes
    @Override
    public void stateChanged(ChangeEvent e)
    {
        //When the slider state changes, getValue displays
        //the value in the label in real time
        label.setText("\u000B°C = "+ slider.getValue());

    }
}
