/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 8: JLayered Pane
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Jlayered Pane demo for J Swing
 * Provides a 3D effect for positioning components
 * */

package sample;

import javax.swing.*;
import java.awt.*;

public class Main
{
    public static void main(String[] args)
    {
        /* Pane */
        //Create the pane
        JLayeredPane layeredPane = new JLayeredPane();
        //Set the pane bounds
        layeredPane.setBounds(0,0,500,500 );

        /* Labels */
        //Create label1
        JLabel label1 = new JLabel("RED");
        //Set opacity
        label1.setOpaque(true);
        //Set the background color
        label1.setBackground(Color.RED);
        //Set the boundaries
        label1.setBounds(50,50,200,200);

        //Create label2
        JLabel label2 = new JLabel("ORANGE");
        //Set opacity
        label2.setOpaque(true);
        //Set the background color
        label2.setBackground(Color.ORANGE);
        //Set the boundaries
        label2.setBounds(100,100,200,200);

        //Create label3
        JLabel label3 = new JLabel("BLUE");
        //Set opacity
        label3.setOpaque(true);
        //Set the background color
        label3.setBackground(Color.BLUE);
        //Set the boundaries
        label3.setBounds(150,150,200,200);

        //Create label4
        JLabel label4 = new JLabel("YELLOW");
        //Set opacity
        label4.setOpaque(true);
        //Set the background color
        label4.setBackground(Color.YELLOW);
        //Set the boundaries
        label4.setBounds(250,250,200,200);


        /* Frame */
        //Create the frame
        JFrame frame = new JFrame("JLayeredPane");
        //Add the layered pane to the frame
        frame.add(layeredPane);
        //Set the close ops
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the frame dimensions
        frame.setSize(500,500);
        //Set the frame layout
        frame.setLayout(null);


        //Add the labels to the layered pane, set the layer order
        layeredPane.add(label1, JLayeredPane.DEFAULT_LAYER);
        layeredPane.add(label2, JLayeredPane.DEFAULT_LAYER);
        layeredPane.add(label3, JLayeredPane.DRAG_LAYER);
        //Set the layer priority using wrapper class and the
        //numerical value for each layer. Higher number = higher placement
        layeredPane.add(label4, Integer.valueOf(4));

        //Set frame visibility
        frame.setVisible(true);


    }
}
