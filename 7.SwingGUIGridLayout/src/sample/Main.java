/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 7: Grid Layout
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Grid layout manager demo for J Swing
 * */

package sample;

import javax.swing.*;
import java.awt.*;

public class Main
{
    public static void main(String[] args)
    {
        /* Frame */
        //Create a frame
        JFrame frame = new JFrame();
        //Set the close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the frame size
        frame.setSize(500,500);
        //Set new layout, pass in grid with rows and columns
        //and spacing in pixels
        frame.setLayout(new GridLayout(3,3,10,10));
        //Set frame visibility
        frame.setVisible(true);

        /* Buttons */
        //Create some buttons and name them
        frame.add(new JButton("1"));
        frame.add(new JButton("2"));
        frame.add(new JButton("3"));
        frame.add(new JButton("4"));
        frame.add(new JButton("5"));
        frame.add(new JButton("6"));
        frame.add(new JButton("7"));
        frame.add(new JButton("8"));
        frame.add(new JButton("9"));
        //frame.add(new JButton("10"));



    }
}
