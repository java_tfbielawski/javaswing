/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 6: Flow Layout
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Flow layout demo for J Swing
 * */

/* Flow Layout will push components to the next row when space runs out */
package sample;

import java.awt.*;
import javax.swing.*;

public class Main
{
    public static void main(String[] args)
    {
        /* Frame */
        //Create the frame
        JFrame frame = new JFrame();
        //Set the exit operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the frame size
        frame.setSize(500,500);
        //Set the flow layout and pass in alignment,
        //horizontal spacing and vertical spacing
        frame.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));

        /* Panels */
        //Create the panel
        JPanel panel = new JPanel();
        //Set the panel size and pass in the dimensions
        panel.setPreferredSize(new Dimension(100,250));
        //Set panel background color
        panel.setBackground(Color.LIGHT_GRAY);
        //Set the panel layout
        panel.setLayout(new FlowLayout());


        /* Components */
        //JButton button1 = new JButton();
        //frame.add(button1);
        // --or--
        panel.add(new JButton("1"));
        panel.add(new JButton("2"));
        panel.add(new JButton("3"));
        panel.add(new JButton("4"));
        panel.add(new JButton("5"));
        panel.add(new JButton("6"));
        panel.add(new JButton("7"));
        panel.add(new JButton("8"));
        panel.add(new JButton("9"));

        //Add the panel
        frame.add(panel);

        //Set visibility
        frame.setVisible(true);


    }
}
