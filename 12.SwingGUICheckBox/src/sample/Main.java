/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 12: Checkboxes
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo checkboxes with a dialog box
 * Main.java
 * */
package sample;


public class Main
{
    public static void main(String[] args)
    {
	//Instantiate a MyFrame class object
        MyFrame frame = new MyFrame();
    }
}
