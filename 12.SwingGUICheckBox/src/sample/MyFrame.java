/* *
 * Tom Bielawski
 * 03/17/2021
 * Bro Code Java GUI Swing Tutorial 12: Checkboxes
 * https://www.youtube.com/watch?v=Kmgo00avvEw
 * Program to demo checkboxes with a dialog box
 * MyFrame.java
 * */

package sample;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener
{
    //Declare a button and a checkbox
    JButton button;
    JCheckBox checkBox;

    //Declare icons
    ImageIcon xIcon;
    ImageIcon checkIcon;

    //Constructor
    MyFrame()
    {
        //Set the close operation
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the layout, pass in FlowLayout
        this.setLayout(new FlowLayout());

        //Instantiate the icons
        xIcon = new ImageIcon("xmark.png");
        checkIcon = new ImageIcon("bluecheck.png");

        //Create the button
        button = new JButton("Push!!");
        //Action listener for the button
        button.addActionListener(this);

        //Create the checkbox
        checkBox = new JCheckBox();
        //Set the text
        checkBox.setText("SEMPER FIDELIS");
        //Set the focus
        checkBox.setFocusable(false);
        //Set the font
        checkBox.setFont(new Font("Droid Serif",Font.BOLD,35));
        //Default icon will be X
        checkBox.setIcon(xIcon);
        //Changes to check when selected
        checkBox.setSelectedIcon(checkIcon);


        //Add the checkbox
        this.add(checkBox);
        //Add the button
        this.add(button);
        //
        this.pack();
        //Set visibility
        this.setVisible(true);

    }

    //Method activated when trigger event occurs
    @Override
    public void actionPerformed(ActionEvent e)
    {
        //Button is pushed..
        if (e.getSource() == button)
        {
            //Show this dialog box to confirm "true" if box checked otherwise "false"
            JOptionPane.showMessageDialog(null, checkBox.isSelected(),
                    "Dialog Title Here",JOptionPane.INFORMATION_MESSAGE);

        }
    }
}
